# Test_code_W2M_Antonio_Carriazo - PRUEBA TÉCNICA FRONT-END

Desarrollo utilizando Angular y Typescript de una aplicación SPA que permite el mantenimiento de una lista de superhéroes.

Se incluyen dos versiones alternativas: utilizando un array de datos y utilizando un mock back-end "In memory API" proporcionado para pruebas por Angular.

# OPCIÓN 1: datos proporcionados por un array.

- Se implementa CRUD conforme a los requerimientos mediante un servicio de acceso a datos y un componente.
- Test unitario del servicio de acceso a datos.
- Test unitario del componente.


# OPCIÓN 2: mock back-end "In memory API".

- Se implementa CRUD conforme a los requerimientos mediante un servicio de acceso a datos y un componente.
- Test unitario del servicio de acceso a datos.
- No se incluye test unitario del componente ya que en esta opción el elemento diferencial es el servicio de acceso a datos. 

# RUTAS.

- En cuanto al routing, se incluye un archivo general de rutas simple en el módulo @config (que incluye también una página 404 not-found). Las subrutas serán cargadas a través de cada módulo de feature.

    · server:port/superheroes/array

    · server:port/superheroes/inmemory

    · 404 not-found page.
    
    · redirección a /superheroes/array.

# Notas de diseño e implementación:

- Se utiliza un diseño flexible y modular intentando seguir en lo posible los principios SOLID, KISS y DRY.

- Código y nomenclaturas autodescriptivas para evitar el uso de comentarios y fomentar el "clean code".

- Dentro de la aplicación se presentan tres niveles (módulos): 

    · @core, que incluye los elementos de la aplicación esenciales, aunque en este caso por la sencillez tiene muy pocas responsabilidades.

    · @shared, que se incluye los elementos que podrían ser reutilizados por diversas "features", tales como servicios, componentes, modelos etc.

    · features, que incluirían cada una de las diversas funcionalidades requeridas en la aplicación, con elementos específicos y no reutilizables por otras features.

    · @config incluye diversos elementos de configuraación como rutas generales, fake data etc.

- Se da importancia a la reutilización de elementos, como pueden ser componentes, servicios o clases:

- Dentro de cada módulo el diseño principal está formado por componente --> container --> page. Además pueden existir servicios, modelos, validadores etc. según requerimientos de cada módulo.

    · un componente es la unidad básica que muestra la información, sin lógica de negocio, y se relaciona con el container en este caso a través de @Inputs y @Outputs.

    · un container contiene la lógica de negocio, acceso a servicios y datos etc. Utiliza los componentes para mostrar la información. 

    · una página no tiene lógica de negocio y simplemente incluye uno o más componentes.

- El app.module no tiene responsabilidades de importancia, delegando esta misión en el resto de módulos principales de la aplicación. Para los módulos de las features se realiza un lazy loading para cargarlos sólamente cuando sean requeridos.

- Se utiliza Angular Material en algunos elementos de las plantillas.

- Se incluye directiva para mostrar mayúsculas en las plantillas.

- Mock server con llamadas http a través de la "In memory API" de Angular, según lo comentado más arriba.

- En la Opción 1 se utiliza programación reactiva desde el servicio a través de observable/subject para comunicación de datos con el componente.

- En la Opción 2 se utiliza programación reactiva como forma de acceso a los datos del mock server a través de http.

- Se utilizan lambdas a lo largo del código según han sido necesarias.

- Se incluye un componente "loading" que se muestra durante la carga de los datos. En la versión de array se "emula" un retardo de 500 ms para poder visualizar la funcionalidad y en la versión "in memory" se gestiona desde la subscripción al observable acorde a la recepción de datos. Éste úlitmo caso sería el que utilizaríamos si dispusiésemos de un back-end Rest API remoto real, con los consiguientes retardos de comunicación.




