import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SuperHeroeInterface } from "src/app/@shared/model/super-heroe.interface";
import { CrudServiceObservableInterface } from '../../model/crud-service-observable.interface';

@Injectable({
  providedIn: 'root',
})
export class RemoteDataService implements CrudServiceObservableInterface {

  private superheroesUrl = 'api/superheroes/';
 
  constructor(private http: HttpClient) { 
    }

  /*   ### API NOT WORKING IN 'create' ###   */
  public create(heroe: SuperHeroeInterface): Observable<SuperHeroeInterface> {
    let headers = new HttpHeaders(
      {
        'Content-Type': 'application/json'
      });
    let options = { headers: headers };
    return this.http.post<SuperHeroeInterface>(this.superheroesUrl, heroe, options);
  }

  public readAll(): Observable<Array<SuperHeroeInterface>> {
    return this.http.get<Array<SuperHeroeInterface>>(this.superheroesUrl);
  }

  public readById(id: number): Observable<SuperHeroeInterface> {
    return this.http.get<SuperHeroeInterface>(`${this.superheroesUrl}${id}`);
  }

  public update(id: number, heroe: SuperHeroeInterface): Observable<SuperHeroeInterface> {
    return this.http.put<SuperHeroeInterface>(`${this.superheroesUrl}${id}`, heroe);
  }

  public delete(id: number): Observable<SuperHeroeInterface> {
    return this.http.delete<SuperHeroeInterface>(`${this.superheroesUrl}${id}`);
  }

}