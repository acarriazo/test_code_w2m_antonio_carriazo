
import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { SuperHeroesData } from '../../../@config/data/super-heroes-data.const';
@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  constructor() { }
  createDb() {
    let superheroes = SuperHeroesData;
    return { superheroes  };
  }
}