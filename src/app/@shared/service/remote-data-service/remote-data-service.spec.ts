import { TestBed, getTestBed } from '@angular/core/testing';
import { RemoteDataService } from './remote-data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SuperHeroeInterface } from '../../model/super-heroe.interface';
import { HttpErrorResponse } from '@angular/common/http';

const dummySuperHeroesList: Array<SuperHeroeInterface> = [
    {
        id: 1,
        name: "Mortadelo",
        superpower: "invincible"
     },
     {
        id: 2,
        name: "Filemón",
        superpower: "stealth"
     },
     {
        id: 3,
        name: "Rompetechos",
        superpower: "speed"
     }
];

const dummySuperHeroe: SuperHeroeInterface = {
    id: 99,
    name: "Carpanta",
    superpower: "intelligence"
};

describe('remote-data-service TEST GROUP 1', () => {
  let injector: TestBed;
  let service: RemoteDataService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ RemoteDataService ]
    });
    injector = getTestBed();
    service = TestBed.inject(RemoteDataService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
    injector = null;
    service = null;
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('create() should return data', () => {
    service.create(dummySuperHeroe).subscribe((res) => {
      expect(res).toEqual(dummySuperHeroe);
    });

    const req = httpMock.expectOne(`api/superheroes/`);
    expect(req.request.method).toBe('POST');
    req.flush(dummySuperHeroe);
  })

  it('readAll() should return data', () => {
    service.readAll().subscribe((res) => {
      expect(res).toEqual(dummySuperHeroesList);
      expect(res).toBeTruthy();
      const response = res.find(response => response.id === 3);
      expect(response.name).toBe(
          "Rompetechos");
    });

    const req = httpMock.expectOne(`api/superheroes/`);
    expect(req.request.method).toBe('GET');
    req.flush(dummySuperHeroesList);
  })

  it('readById() should return data', () => {
    service.readById(3).subscribe((res: SuperHeroeInterface) => {
      expect(res).toEqual(dummySuperHeroe);
      expect(res).toBeTruthy();
      expect(res.id).toBe(99);
    })

    const req = httpMock.expectOne(`api/superheroes/3`);
    expect(req.request.method).toBe('GET');
    req.flush(dummySuperHeroe);
  })

  it('update() should return data', () => {
    service.update(3, dummySuperHeroe).subscribe((res: SuperHeroeInterface) => {
      expect(res).toEqual(dummySuperHeroe);
      expect(res).toBeTruthy();
      expect(res.id).toBe(99);
    })

    const req = httpMock.expectOne(`api/superheroes/3`);
    expect(req.request.method).toBe('PUT');
    req.flush(dummySuperHeroe);
  })


  it('update() should send an error if update fails', () => {
    service.update(3, dummySuperHeroe).subscribe({
        next: () => {
            () => fail("the save course operation should have failed");
        },
        error: (error: HttpErrorResponse)  => {
            expect(error.status).toBe(500);
        }
      })
    const req = httpMock.expectOne(`api/superheroes/3`);
    expect(req.request.method).toBe('PUT');
    req.flush(dummySuperHeroe);
  }) 

  it('delete() should return data', () => {
    service.delete(2).subscribe((res: SuperHeroeInterface) => {
      expect(res).toEqual(dummySuperHeroe);
      expect(res).toBeTruthy();
      expect(res.id).toBe(99);
    })

    const req = httpMock.expectOne(`api/superheroes/2`);
    expect(req.request.method).toBe('DELETE');
    req.flush(dummySuperHeroe);
  })

});




