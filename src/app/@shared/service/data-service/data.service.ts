import { Injectable } from "@angular/core";
import { CrudServiceArrayInterface } from '../../model/crud-service-array.interface';
import { SuperHeroesData } from "src/app/@config/data/super-heroes-data.const";
import { SuperHeroeInterface } from "../../model/super-heroe.interface";
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class DataService implements CrudServiceArrayInterface {

  public superheroes: Array<SuperHeroeInterface>;
  public dataServiceSubject$!: Subject<Array<SuperHeroeInterface>>;
  public superheroesFilter: Array<number>;
 
  constructor() {  
    this.superheroesFilter = [];
    this.superheroes = SuperHeroesData;
    this.dataServiceSubject$ = new Subject();
    }

  public create(heroe: SuperHeroeInterface): void {
    this.superheroesFilter = [];
    this.superheroes.push(heroe);
    this.dataServiceSubject$.next(this.superheroes);
  }

  public readAll(): void {
    this.superheroesFilter = [];
    this.dataServiceSubject$.next(this.superheroes);
  }

  public readById(id: number): void {
    this.superheroesFilter = [];
    let foundHeroe = this.superheroes.find(heroe => heroe.id === id);

    if(foundHeroe) {
      this.superheroesFilter.push(foundHeroe.id);
      this.applyFilter();
    } else {
      this.dataServiceSubject$.next([]);
    }
  }


  public readByValue(value: string): void {
    this.superheroesFilter = [];
    let foundHeroes: Array<SuperHeroeInterface> = [];

    if(value.length) {
      this.superheroes.forEach((heroe) => {
        let areEqual: boolean = heroe.name.toUpperCase().includes(value.toUpperCase());
        if(areEqual) {
          foundHeroes.push(heroe);
          this.superheroesFilter.push(heroe.id);
        }
      });
    }

    if(foundHeroes.length) {
      this.applyFilter();
    } else {
      this.dataServiceSubject$.next([]);
    }
  }  

  public update(id: number, updatedHeroe: SuperHeroeInterface): void  {
    let foundHeroe: SuperHeroeInterface;

    this.superheroes.forEach((heroe, index) => {
      if(heroe.id === id) {
        this.superheroes[index] = updatedHeroe;
        foundHeroe = this.superheroes[index];
      }
    });
    this.applyFilter();
  }

  public delete(id: number): void {
    this.superheroes.forEach((heroe, index) => {
      if(heroe.id === id) {
        this.superheroes.splice(index,1);
      }
    });
    this.applyFilter();
  }

  public applyFilter(): void {
    let filteredSuperheroes: Array<SuperHeroeInterface> = [];

    if(!this.superheroesFilter.length) {
      this.dataServiceSubject$.next(this.superheroes);
    } else {
      this.superheroesFilter.forEach(
        (id: number) => {
          let foundHeroe = this.superheroes.find(heroe => heroe.id === id);
          if(foundHeroe) {
            filteredSuperheroes.push(foundHeroe);
          }
        }
      );
      this.dataServiceSubject$.next(filteredSuperheroes);
    }
  }

}