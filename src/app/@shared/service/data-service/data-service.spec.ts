import { TestBed, getTestBed } from '@angular/core/testing';
import { DataService } from './data.service';

import { 
  dummySuperHeroeArray, 
  dummySuperHeroeFilteredArray, 
  dummySuperHeroe, 
  dummySuperHeroeUpdated, 
  dummySuperHeroeDeleted 
} from '../../../@config/data/data-service-test-fake-data';

describe('data-service TEST GROUP 1', () => {
  let injector: TestBed;
  let dataService: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ DataService ]
    });
    injector = getTestBed();
    dataService = TestBed.inject(DataService);
    dataService.superheroes = dummySuperHeroeArray;
  });

  afterEach(() => {
    injector = null;
    dataService = null;
  })

  it('should constructor()', () => {
    expect(dataService).not.toBeNull();
    expect(dataService).toBeTruthy();
    expect([]).toEqual(dataService.superheroesFilter);
    expect(dataService.superheroes).toEqual(dummySuperHeroeArray);
    expect(dataService.dataServiceSubject$).not.toBeNull();
  });

  it('should create()', () => {
    
    dataService.dataServiceSubject$.subscribe(data => {
      dummySuperHeroeArray.push(dummySuperHeroe);
      expect(data).toEqual(dummySuperHeroeArray);
    });
    dataService.create(dummySuperHeroe);
    expect(dataService.superheroes).toContain(dummySuperHeroe);

    const methodSpy = spyOn(dataService, 'create');
    dataService.create(dummySuperHeroe);
    expect(methodSpy).toHaveBeenCalled();
    expect([]).toEqual(dataService.superheroesFilter);
  })

  it('should readAll()', () => {
    
    dataService.dataServiceSubject$.subscribe(data => {
      expect(data).toEqual(dummySuperHeroeArray);
    });
    dataService.readAll();
    

    const methodSpy = spyOn(dataService, 'readAll');
    dataService.readAll();
    expect(methodSpy).toHaveBeenCalled();
    expect([]).toEqual(dataService.superheroesFilter);
  })

  it('should readById() found', () => {

    /* ORDER MATTERS, methodSpy2 must be executed before methodSpy1 OR TEST FAILS */

    const methodSpy2 = spyOn(dataService, 'applyFilter');
    dataService.readById(3);
    expect(methodSpy2).toHaveBeenCalled(); 

    dataService.readById(3);
    expect(dataService.superheroesFilter).toContain(3);
   
    const methodSpy1 = spyOn(dataService, 'readById');
    dataService.readById(3);
    expect(methodSpy1).toHaveBeenCalled();
  })

  it('should readById() not found', () => {
    
    dataService.dataServiceSubject$.subscribe(data => {
      expect(data).toEqual([]);
    });
    dataService.readById(99);
    

    dataService.readById(99);
    expect(dataService.superheroesFilter).toEqual([]);

    const methodSpy = spyOn(dataService, 'readById');
    dataService.readById(99);
    expect(methodSpy).toHaveBeenCalled();
  })


  it('should readByValue() found', () => {
    const methodSpy2 = spyOn(dataService, 'applyFilter');
    dataService.readByValue('Spiderman');
    expect(methodSpy2).toHaveBeenCalled();

    dataService.readByValue('Spiderman');
    expect(dataService.superheroesFilter).toContain(2);

    const methodSpy = spyOn(dataService, 'readByValue');
    dataService.readByValue('Spiderman');
    expect(methodSpy).toHaveBeenCalled();
  })

  it('should readByValue() not found', () => {
    dataService.dataServiceSubject$.subscribe(data => {
      expect(data).toEqual([]);
    });
    dataService.readByValue('Asterix');

    dataService.readByValue('Asterix');
    expect(dataService.superheroesFilter).toEqual([]);

    const methodSpy = spyOn(dataService, 'readByValue');
    dataService.readByValue('Asterix');
    expect(methodSpy).toHaveBeenCalled();
  }) 


  it('should update()', () => {
    const methodSpy2 = spyOn(dataService, 'applyFilter');
    dataService.update(4, dummySuperHeroe);
    expect(methodSpy2).toHaveBeenCalled();

    dataService.update(4, dummySuperHeroe);
    expect(dataService.superheroes).toContain(dummySuperHeroe);
    expect(dataService.superheroes).not.toContain(dummySuperHeroeUpdated);

    const methodSpy = spyOn(dataService, 'update');
    dataService.update(4, dummySuperHeroe);
    expect(methodSpy).toHaveBeenCalled(); 
  })

  it('should delete()', () => {
    const methodSpy2 = spyOn(dataService, 'applyFilter');
    dataService.delete(7);
    expect(methodSpy2).toHaveBeenCalled();

    dataService.delete(7);
    expect(dataService.superheroes).not.toContain(dummySuperHeroeDeleted);

    const methodSpy = spyOn(dataService, 'delete');
    dataService.delete(7);
    expect(methodSpy).toHaveBeenCalled(); 
  })

  it('should applyFilter() hasfilter', () => {
    dataService.dataServiceSubject$.subscribe(data => {
      expect(data).toEqual(dummySuperHeroeFilteredArray);
    });
    dataService.superheroesFilter = [9, 10, 11];

    const methodSpy = spyOn(dataService, 'applyFilter');
    dataService.applyFilter();
    expect(methodSpy).toHaveBeenCalled(); 
  })

  it('should applyFilter() does not have filter', () => {
    dataService.dataServiceSubject$.subscribe(data => {
      expect(data).toEqual([]);
    });
    dataService.superheroesFilter = [];

    const methodSpy = spyOn(dataService, 'applyFilter');
    dataService.applyFilter();
    expect(methodSpy).toHaveBeenCalled(); 
  })

});





