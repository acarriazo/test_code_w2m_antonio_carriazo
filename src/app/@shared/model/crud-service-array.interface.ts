import { SuperHeroeInterface } from "./super-heroe.interface";
export interface CrudServiceArrayInterface {
    create(heroe: SuperHeroeInterface): void;
    readAll(): void;
    readById(id: number): void;
    readByValue(value: string): void;
    update(id: number, heroe: SuperHeroeInterface): void;
    delete(id: number): void;
    applyFilter(): void;
}