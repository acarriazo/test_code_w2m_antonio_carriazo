export enum FormAction {
    create = 'create',
    edit = 'edit',
    none = 'none',
}