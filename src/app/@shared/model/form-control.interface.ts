import { FormControl } from '@angular/forms';

export interface FormControlInterface {
    [index: string]: FormControl,
}