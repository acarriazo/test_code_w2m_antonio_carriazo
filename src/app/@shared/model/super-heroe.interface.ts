import { SuperpowerType } from '../../super-heroes/model/super-power.type';
export interface SuperHeroeInterface {
    id: number,
    name: string,
    superpower: SuperpowerType
}