export interface NotNullValidatorResponseInterface {
    [index: string]: boolean | string,
};

export type ValidatorResponseType = NotNullValidatorResponseInterface | null;
