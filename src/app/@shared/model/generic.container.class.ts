import { RemoteDataService } from '../service/remote-data-service/remote-data.service';
import { LoaderService } from "../../@core/service";
import { Observable, Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoaderServiceStatus } from '.';
import { SuperHeroeInterface } from './super-heroe.interface';

export class GenericContainerClass {

    public title: string;
    public readAllItemsArrayObservable$!: Observable<Array<SuperHeroeInterface>>;
    public readSomeItemsArrayObservable$!: Observable<SuperHeroeInterface>;
    public readAllItemsArrayObservableSubscription!: Subscription;
    public readItemByIdArrayObservableSubscription!: Subscription;
    public itemsArray!: Array<SuperHeroeInterface>;
    public superheroesFilter: Array<number>;
 
    constructor(public dataService: RemoteDataService, public loaderService: LoaderService)
    {
        this.title = 'Item Container';
        this.resetFilter();
        this.readAll();
    }

    public create(item: SuperHeroeInterface): void {
        this.readSomeItemsArrayObservable$ = this.dataService.create(item);
        this.readItemByIdArrayObservableSubscription = this.readSomeItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: () => {   
                this.resetFilter();      
                this.readAll();
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });        
    }

    public readAll(): void {
        this.loaderService.emitLoadStatus(LoaderServiceStatus.visible);
        this.readAllItemsArrayObservable$ = this.dataService.readAll();
        this.readAllItemsArrayObservableSubscription = this.readAllItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: (data: Array<SuperHeroeInterface>) => {
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
                this.resetFilter();
                this.applyFilter(data);
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    }

    public readSome(): void {
        this.loaderService.emitLoadStatus(LoaderServiceStatus.visible);
        this.readAllItemsArrayObservable$ = this.dataService.readAll();
        this.readAllItemsArrayObservableSubscription = this.readAllItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: (data: Array<SuperHeroeInterface>) => {
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
                this.applyFilter(data);
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    } 
     

    public readById(id: number): void {
        this.loaderService.emitLoadStatus(LoaderServiceStatus.visible);
        this.readSomeItemsArrayObservable$ = this.dataService.readById(id);
        this.readItemByIdArrayObservableSubscription = this.readSomeItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: (data: SuperHeroeInterface) => {
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
                this.resetFilter();
                this.superheroesFilter.push(id);
                this.applyFilter([data]);
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    }  
    
    public readByValue(value: string): void {
        this.loaderService.emitLoadStatus(LoaderServiceStatus.visible);
        this.readAllItemsArrayObservable$ = this.dataService.readAll();
        this.readAllItemsArrayObservableSubscription = this.readAllItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: (data: Array<SuperHeroeInterface>) => {
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);   
                this.resetFilter();    
                data.forEach((heroe) => {
                    let areEqual: boolean = heroe.name.toUpperCase().includes(value.toUpperCase());
                    if(areEqual) {
                        this.superheroesFilter.push(heroe.id);
                    }
                  });
                this.applyFilter(data);
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    }    

    public update(id: number, item: SuperHeroeInterface): void {
        this.readSomeItemsArrayObservable$ = this.dataService.update(id, item);
        this.readItemByIdArrayObservableSubscription = this.readSomeItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: () => {
                this.readSome();
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    }

    public delete(id: number): void {
        this.readSomeItemsArrayObservable$ = this.dataService.delete(id);
        this.readItemByIdArrayObservableSubscription = this.readSomeItemsArrayObservable$
        .pipe(
            catchError(err => {
                console.error(err);
                return throwError(() => new Error());
            })
        )       
        .subscribe({
            next: () => {
                this.readSome();
            },
            error: (error:any) => {
                console.error(error);
                this.loaderService.emitLoadStatus(LoaderServiceStatus.notVisible);
            },
            complete: () => { 
            }
        });
    }

    public resetFilter(): void {
        this.superheroesFilter = [];
    }

    public applyFilter(data: Array<SuperHeroeInterface>): void {
        let filteredSuperheroes: Array<SuperHeroeInterface> = [];
        this.itemsArray = [];
      
        if(!this.superheroesFilter.length) {
            this.itemsArray = data;
        } else {
          this.superheroesFilter.forEach(
            (id: number) => {
              let foundHeroe = data.find(heroe => heroe.id === id);
              if(foundHeroe) {
                filteredSuperheroes.push(foundHeroe);
              }
            }
          );
          this.itemsArray = filteredSuperheroes;
        }
      }

    public unsubscribeObservables(): void {
        if (this.readAllItemsArrayObservableSubscription) {
            this.readAllItemsArrayObservableSubscription.unsubscribe();
        }
        if (this.readItemByIdArrayObservableSubscription) {
            this.readItemByIdArrayObservableSubscription.unsubscribe();
        } 
    }

}