export interface FormCustomFieldInterface {
    name: string,
    type: string,
    value: string | number | null,
    validators: Array<Function>,
    disabled: boolean
}

