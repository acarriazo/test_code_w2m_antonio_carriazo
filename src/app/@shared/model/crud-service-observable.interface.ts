import { SuperHeroeInterface } from "./super-heroe.interface";
import { Observable } from "rxjs";

export interface CrudServiceObservableInterface {
    create(heroe: SuperHeroeInterface): Observable<SuperHeroeInterface>;
    readAll(): Observable<Array<SuperHeroeInterface>>;
    readById(id: number): Observable<SuperHeroeInterface>;
    update(id: number, heroe: SuperHeroeInterface): Observable<SuperHeroeInterface>;
    delete(id: number): Observable<SuperHeroeInterface>;

}