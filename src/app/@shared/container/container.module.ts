import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedComponentModule } from '../component/component.module';

@NgModule({
  declarations: [],
  imports: [SharedComponentModule, CommonModule],
  providers: [],
  exports: []
})
export class ContainerModule { }