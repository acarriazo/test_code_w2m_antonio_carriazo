import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageNotFoundPage } from '.';
import { ContainerModule } from '../container/container.module';

@NgModule({
  declarations: [
    PageNotFoundPage,
  ],
  imports: [CommonModule, ContainerModule],
  providers: [],
  exports: [ 
    PageNotFoundPage,
  ]
})

export class PageModule { }