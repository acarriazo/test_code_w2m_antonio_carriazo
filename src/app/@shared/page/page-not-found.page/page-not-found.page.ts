import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found-page',
  templateUrl: './page-not-found.page.html',
  styleUrls: ['./page-not-found.page.scss']
})
export class PageNotFoundPage implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
