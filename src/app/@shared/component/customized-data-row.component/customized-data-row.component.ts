import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-customized-data-row-component',
    templateUrl: './customized-data-row.component.html',
    styleUrls: ['./customized-data-row.component.scss']
})
export class CustomizedDataRowComponent implements OnChanges {

    @Input() public inputArray!: Array<any>;
    public inputArrayLength: number;
    @Output() public outputUpdateClick: EventEmitter<number>;
    @Output() public outputDeleteClick: EventEmitter<number>;

    constructor() {
        this.inputArrayLength = 0;
        this.outputUpdateClick = new EventEmitter<number>;
        this.outputDeleteClick = new EventEmitter<number>;
    }

    public update(id: number): void {
        this.outputUpdateClick.emit(id);
    }

    public delete(id: number): void {
        this.outputDeleteClick.emit(id);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['inputArray'].currentValue !== undefined) {
            this.inputArrayLength = this.inputArray.length;
        }
    }

}