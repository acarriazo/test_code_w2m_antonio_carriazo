import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormControlInterface, FormCustomFieldInterface } from '../../model';

@Component({
    selector: 'app-customized-form-component',
    templateUrl: './customized-form.component.html',
    styleUrls: ['./customized-form.component.scss']
})
export class CustomizedFormComponent implements OnChanges {
  
    @Input() public customizedFormTitle: string;
    @Input() public customizedFormLogo: string;
    @Input() public customizedFormText: string;
    public customizedForm: FormGroup;
    @Input() public isFormModal: boolean;
    @Input() public isFormOpen: boolean;
    @Input() public hasCloseButton: boolean;
    public controls: FormControlInterface;
    @Input() public customFields: Array<FormCustomFieldInterface>;
    @Output() public updatedFieldValues: EventEmitter<any>;
    @Output() public hasBeenFormClosed: EventEmitter<boolean>;

    constructor() {
        this.customizedFormTitle = 'My Customized Form';
        this.customizedFormLogo = '';
        this.customizedFormText = '';
        this.isFormModal = false;
        this.isFormOpen = false;
        this.hasCloseButton = true;
        this.controls = {};
        this.customFields = [];      
        this.customizedForm = new FormGroup(this.controls);
        this.updatedFieldValues = new EventEmitter<any>;
        this.hasBeenFormClosed = new EventEmitter<boolean>;
    }

    public get customizedFormControls() {
        return this.customizedForm.controls;
      }

    public onCloseForm(): void {
        this.isFormOpen = false;
        this.hasBeenFormClosed.emit(this.isFormOpen);
    }

    public pupulateFormcontrols(): void {
        this.customFields.forEach(
            (fieldName: any) => {     
                    this.controls = { 
                        ...this.controls, 
                        [fieldName.name]: new FormControl(fieldName.value, fieldName.validators)
                    }           
            }
        );
    }

    public onSubmitForm(): void {
        this.updatedFieldValues.emit(this.customizedForm.value);
        this.customizedForm.reset();
        this.onCloseForm();
    };

    ngOnChanges(): void {
            this.pupulateFormcontrols();
            this.customizedForm = new FormGroup(this.controls);
    }

}