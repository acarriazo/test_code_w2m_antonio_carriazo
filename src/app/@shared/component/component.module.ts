import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DirectiveModule } from '../directive/directive.module';
import { SpinnerComponent } from '.';
import { CustomizedButtonComponent } from '.';
import { CustomizedDataTableComponent } from '.';
import { CustomizedFormComponent } from '.';
import { NgxPaginationModule} from 'ngx-pagination'; 
import { MatBadgeModule } from '@angular/material/badge';

@NgModule({
  declarations: [
    CustomizedButtonComponent,  
    CustomizedDataTableComponent,
    CustomizedFormComponent,
    SpinnerComponent,
  ],
  imports: [ 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DirectiveModule,
    NgxPaginationModule,
    MatBadgeModule
   ],
  providers: [],
  exports: [ 
    CustomizedButtonComponent,
    CustomizedDataTableComponent,
    CustomizedFormComponent,
    DirectiveModule,
    SpinnerComponent
   ]
})

export class SharedComponentModule { }