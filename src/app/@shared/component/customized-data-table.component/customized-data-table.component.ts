import { Component, 
    Input, 
    Output, 
    EventEmitter, 
    OnChanges, 
    SimpleChanges,  
    DoCheck, 
    ChangeDetectorRef
} from '@angular/core';

@Component({
    selector: 'app-customized-data-table-component',
    templateUrl: './customized-data-table.component.html',
    styleUrls: ['./customized-data-table.component.scss'],
})
export class CustomizedDataTableComponent implements OnChanges, DoCheck {

    @Input() public inputArray: Array<any> = [];
    public inputArrayLength: number = 0;
    public inputArrayFieldNames: Array<string>;
    @Input() public updateButtonVisible: boolean;
    @Input() public deleteButtonVisible: boolean;
    @Output() public outputUpdateClick: EventEmitter<number>;
    @Output() public outputDeleteClick: EventEmitter<number>;

    public page: number = 1;
    public itemsPerPage: number = 5;
  
    constructor(private changeDetectoRef: ChangeDetectorRef) {
        this.inputArrayLength = 0;
        this.inputArrayFieldNames = [];
        this.updateButtonVisible = true;
        this.deleteButtonVisible = true;
        this.outputUpdateClick = new EventEmitter<number>;
        this.outputDeleteClick = new EventEmitter<number>;
    }

    public update(id: number): void {
        this.inputArray.forEach(item => {
            if(item.id === id) {
                this.outputUpdateClick.emit(item);
            }
        });
    }

    public delete(id: number): void {
        this.outputDeleteClick.emit(id);
    }

    public getInputArrayFieldNames(): void {
        this.inputArrayFieldNames = [];
        for ( let fieldName in this.inputArray[0] ) {
            this.inputArrayFieldNames.push(fieldName);
        }
    }

    ngDoCheck(): void {
        if(this.inputArray) {
            this.inputArrayLength = this.inputArray.length;
        }
        this.changeDetectoRef.detectChanges();
    }


    ngOnChanges(changes: SimpleChanges): void {
        if (changes['inputArray'].currentValue !== undefined) {
            this.getInputArrayFieldNames();
            this.page = 1;
        }
    }

}