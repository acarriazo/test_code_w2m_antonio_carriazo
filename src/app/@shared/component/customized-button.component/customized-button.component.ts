import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-customized-button-component',
  templateUrl: './customized-button.component.html',
  styleUrls: ['./customized-button.component.scss']
})
export class CustomizedButtonComponent implements OnInit {

  @Input() public inputTitle: string;
  @Output() public outputClick: EventEmitter<boolean>;

  constructor() {
    this.inputTitle = '';
    this.outputClick = new EventEmitter<boolean>;
   }

  ngOnInit(): void {
  }

  onClick(): void {
    this.outputClick.emit(true);
  }


}
