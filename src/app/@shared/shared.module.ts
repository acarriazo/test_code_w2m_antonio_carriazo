import { NgModule } from '@angular/core';
import { SharedComponentModule } from './component/component.module';
import { PageModule } from './page/page.module';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './service';

@NgModule({
  declarations: [],
  imports: [ 
    PageModule, 
    SharedComponentModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService),
  ],
  exports: [
    SharedComponentModule,
    PageModule,
    ],
  providers: [],
})

export class SharedModule { }