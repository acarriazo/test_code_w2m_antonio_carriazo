import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
    selector: '[capitalizeDirective]',
})
export class CapitalizeDirective {

    constructor(private element: ElementRef, private render: Renderer2) {
      this.render.setStyle(this.element.nativeElement, "text-transform", 'uppercase');
      element.nativeElement.style.color = 'uppercase';
     }

   }