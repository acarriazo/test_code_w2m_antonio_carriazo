import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizeDirective } from '.';

@NgModule({
  declarations: [
    CapitalizeDirective
  ],
  imports: [ 
    CommonModule,
  ],
  providers: [],
  exports: [ 
    CapitalizeDirective
   ]
})
export class DirectiveModule { }