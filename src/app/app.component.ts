import { Component, OnDestroy } from '@angular/core';
import { AppComponentAdvancedClass } from './@core/model';
import { LoaderService } from  './@core/service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AppComponentAdvancedClass implements OnDestroy {

  constructor(
    loaderService: LoaderService, 
    ) {
        super(loaderService);
  }

  ngOnDestroy(): void {
      this.unsubscribeObservables();
  }

}
