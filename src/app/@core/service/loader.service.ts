import { Injectable } from "@angular/core";
import { Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService{

  public loaderServiceSubject$!: Subject<boolean>;


  constructor() {
    this.loaderServiceSubject$ = new Subject();
  }

  public getLoadStatus(): Subject<boolean> {
    return this.loaderServiceSubject$;
  }

  public emitLoadStatus(status: string): void {
    if(status === 'visible') {
      this.loaderServiceSubject$.next(true);
    }
    else {
      this.loaderServiceSubject$.next(false);
    }
  
  }

}