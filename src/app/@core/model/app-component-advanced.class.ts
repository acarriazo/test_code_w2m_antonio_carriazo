import { LoaderService } from '../service';
import { Subject, Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

export class AppComponentAdvancedClass {

  public isSpinnerVisible: boolean = false;
  public loaderServiceSubject$!: Subject<boolean>;
  public loaderServiceObservableSubscription!: Subscription;

  constructor(private loaderService: LoaderService) {
    this.getLoadStatus();
  }

  public getLoadStatus(): void {
    this.loaderServiceSubject$ = this.loaderService.getLoadStatus();
    this.loaderServiceObservableSubscription = this.loaderServiceSubject$
      .pipe(
        catchError(err => {
          console.error(err);
          return throwError(() => new Error())
        })
      )
      .subscribe({
        next: (isSpinnerVisible: boolean) => {
          this.isSpinnerVisible = isSpinnerVisible;
        },
        error: (error: any) => {
          console.error(error)
        },
        complete: () => {
        }
      });
  }

  public unsubscribeObservables(): void {
    if (this.loaderServiceObservableSubscription) {
      this.loaderServiceObservableSubscription.unsubscribe();
    }
  }

}
