import { Routes } from '@angular/router';
import { PageNotFoundPage } from '../../@shared/page';

export const routesUser: Routes = [
  
  {
    path: 'superheroes',
    loadChildren: () => import('../../super-heroes/super-heroes.module').then(moduleToLoad => moduleToLoad.SuperHeroesModule),
    title: 'Superheroes'
  }
];

export const routesSystem: Routes = [

  { path: '', redirectTo: 'superheroes/array', pathMatch: 'full' },
  { path: '**', component: PageNotFoundPage, title: 'Upss ... #Error 404' }
];

export const routes: Routes = [...routesUser,  ...routesSystem];
