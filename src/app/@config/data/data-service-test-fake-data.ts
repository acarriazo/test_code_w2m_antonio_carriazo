import { SuperHeroeInterface } from '../../@shared/model/super-heroe.interface';

export const dummySuperHeroeArray: Array<SuperHeroeInterface> =  [
    {
       id: 1,
       name: "Superman",
       superpower: "invincible"
    },
    {
       id: 2,
       name: "Spiderman",
       superpower: "stealth"
    },
    {
       id: 3,
       name: "Increible Hulk",
       superpower: "invincible"
    },
    {
       id: 4,
       name: "Batman",
       superpower: "intelligence"
    },
    {
       id: 5,
       name: "Batgirl",
       superpower: "intelligence"
    },
    {
       id: 6,
       name: "Magneto",
       superpower: "magnetism"
    },
    {
       id: 7,
       name: "Cat Woman",
       superpower: "stealth"
    },
    {
       id: 8,
       name: "Ironman",
       superpower: "intelligence"
    },
    {
       id: 9,
       name: "Captain America",
       superpower: "invincible"
    },
    {
       id: 10,
       name: "Hellboy",
       superpower: "strength"
    },
    {
       id: 11,
       name: "Wolverine",
       superpower: "inmortal"
    },
    {
       id: 12,
       name: "Elektra",
       superpower: "magnetism"
    }
 ]

 export const dummySuperHeroeFilteredArray: Array<SuperHeroeInterface> =  [
   {
      id: 9,
      name: "Captain America",
      superpower: "invincible"
   },
   {
      id: 10,
      name: "Hellboy",
      superpower: "strength"
   },
   {
      id: 11,
      name: "Wolverine",
      superpower: "inmortal"
   }
] 

export const dummySuperHeroe: SuperHeroeInterface = 
{
    id: 66,
    name: "Carpanta",
    superpower: "intelligence"
};

export const dummySuperHeroeUpdated: SuperHeroeInterface =     
{
  id: 4,
  name: "Batman",
  superpower: "intelligence"
};

export const dummySuperHeroeDeleted: SuperHeroeInterface =     
{
  id: 7,
  name: "Cat Woman",
  superpower: "stealth"
};

export const dummySuperHeroeArrayById: Array<SuperHeroeInterface> = [
   {
     id: 3,
     name: "Increible Hulk",
     superpower: "invincible"
   }
 ]
 
export  const dummySuperHeroeArrayByValue: Array<SuperHeroeInterface> = [
   {
     id: 4,
     name: "Batman",
     superpower: "intelligence"
   },
   {
     id: 5,
     name: "Batgirl",
     superpower: "intelligence"
   }
 ]