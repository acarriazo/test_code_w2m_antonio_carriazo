import { SuperHeroeInterface } from "src/app/@shared/model/super-heroe.interface"

export let SuperHeroesData: Array<SuperHeroeInterface> =
   [
      {
         id: 1,
         name: "Superman",
         superpower: "invincible"
      },
      {
         id: 2,
         name: "Spiderman",
         superpower: "stealth"
      },
      {
         id: 3,
         name: "Increible Hulk",
         superpower: "invincible"
      },
      {
         id: 4,
         name: "Batman",
         superpower: "intelligence"
      },
      {
         id: 5,
         name: "Batgirl",
         superpower: "intelligence"
      },
      {
         id: 6,
         name: "Magneto",
         superpower: "magnetism"
      },
      {
         id: 7,
         name: "Cat Woman",
         superpower: "stealth"
      },
      {
         id: 8,
         name: "Ironman",
         superpower: "intelligence"
      },
      {
         id: 9,
         name: "Captain America",
         superpower: "invincible"
      },
      {
         id: 10,
         name: "Hellboy",
         superpower: "strength"
      },
      {
         id: 11,
         name: "Wolverine",
         superpower: "inmortal"
      },
      {
         id: 12,
         name: "Elektra",
         superpower: "magnetism"
      }
   ]
