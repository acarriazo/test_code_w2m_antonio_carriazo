import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuperHeroesArrayPage } from './page';
import { SuperHeroesInMemoryPage } from './page/super-heroes-in-memory.page/super-heroes-in-memory.page';

export const routesUser: Routes = [
    {
        path: 'array',
        component: SuperHeroesArrayPage
    },
    {
      path: 'inmemory',
      component: SuperHeroesInMemoryPage
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routesUser)],
  exports: [RouterModule]
})
export class SuperHeroesRoutingModule {
 }
