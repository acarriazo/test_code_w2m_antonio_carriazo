import { NgModule } from '@angular/core';
import { SharedComponentModule } from '../../@shared/component/component.module';
import { CustomizedButtonComponent } from '../../@shared/component/customized-button.component/customized-button.component';
import { CustomizedDataTableComponent } from '../../@shared/component/customized-data-table.component/customized-data-table.component';
import { CustomizedFormComponent } from '../../@shared/component/customized-form.component/customized-form.component';

@NgModule({
  declarations: [ ],
  imports: [ 
    SharedComponentModule
  ],
  providers: [],
  exports: [ 
    CustomizedButtonComponent,  
    CustomizedDataTableComponent,
    CustomizedFormComponent 
  ]
})
export class ComponentModule { }