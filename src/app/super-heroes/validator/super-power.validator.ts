import { AbstractControl } from '@angular/forms';
import { ValidatorResponseType } from '../../@shared/model';
import { SuperpowerType } from '../model';


export class SuperPowerValidator {
    private static categories: Array<SuperpowerType> = [
        'invisibility',
        'strength',
        'speed',
        'intelligence',
        'stealth',
        'magnetism',
        'invincible'
    ];
    private static allowedCategoriesSet = new Set<SuperpowerType>(SuperPowerValidator.categories);
    private static correctValues: string = `Right values must be: ${SuperPowerValidator.categories}`;

    public static superPower(control: AbstractControl): ValidatorResponseType {
        if (SuperPowerValidator.isEmptyValue(control.value)) {
            return null;
        }
        if (!SuperPowerValidator.allowedCategoriesSet.has(control.value)) {
            return { 'isIncorrect': true, 'correctValues': SuperPowerValidator.correctValues };
        }

        return null;
    }

    public static isEmptyValue(value: string): boolean {
        return value == null || (typeof value === 'string' && value.length === 0);
    }
}
