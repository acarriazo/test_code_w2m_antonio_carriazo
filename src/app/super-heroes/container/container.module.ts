import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperHeroesArrayContainer, SuperHeroesInMemoryContainer } from '.';
import { ComponentModule } from '../component/component.module';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
  declarations: [
    SuperHeroesArrayContainer, SuperHeroesInMemoryContainer
    ],
  imports: [ ComponentModule, MatGridListModule, CommonModule ],
  providers: [],
  exports: [ 
    SuperHeroesArrayContainer, SuperHeroesInMemoryContainer
  ]
})
export class ContainerModule { }