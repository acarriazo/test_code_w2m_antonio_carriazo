import { Component, OnDestroy } from "@angular/core";
import { RemoteDataService } from "../../../@shared/service/remote-data-service/remote-data.service";
import { LoaderService } from "../../../@core/service";
import { SuperHeroeCustomFields } from '../../model';
import {
    FormAction,
    FormCustomFieldInterface,
    GenericContainerClass
} from '../../../@shared/model';
import { SuperHeroeInterface } from "src/app/@shared/model/super-heroe.interface";

@Component({
    selector: 'app-super-heroes-in-memory-container',
    templateUrl: './super-heroes-in-memory.container.html',
    styleUrls: ['./super-heroes-in-memory.container.scss']
})

export class SuperHeroesInMemoryContainer extends GenericContainerClass implements OnDestroy {

    public customizedFormTitle: string;
    public customFields: Array<FormCustomFieldInterface>;
    public isFormModal: boolean; 
    public isFormOpen: boolean = false;; 
    public customizedFormAction: FormAction;
    public customizedFormItemToModify!: SuperHeroeInterface;

    public customizedFormItemToDelete: number;
    public isConfirmDeleteFormOpen: boolean = false;

    constructor(dataService: RemoteDataService, loaderService: LoaderService) 
        {
        super(dataService, loaderService);
        this.title = 'SuperHeroe In Memory Container';
        this.customizedFormTitle = '';
        this.customFields = SuperHeroeCustomFields;
        this.isFormModal = true;
        this.isFormOpen = false;
        this.customizedFormAction = FormAction.none;
   
        this.customizedFormItemToDelete = 0;
        this.isConfirmDeleteFormOpen = false;
    }

    ngOnDestroy(): void {
        this.unsubscribeObservables();
    }

    public createSuperheroe(): void {
       this.customizedFormTitle = 'Create superheroe';
       this.isFormOpen = true;
       this.customizedFormAction = FormAction.create;
       this.customFields.forEach((customField => 
        {
            if(customField.name === 'id'){ 
                customField.value = 'new'
            } else if(customField.name === 'name'){
                customField.value = '';
            } else if(customField.name === 'superpower'){
                customField.value = '';
            }
        }
        ));
    }

    public readSuperheroeById(idString: string): void {
        var idNumber: number = +idString;
        this.readById(idNumber);
    }

    public readSuperheroeByValue(value: string): void {
        this.readByValue(value);
    }

    public updateSuperheroe(superheroe: any): void {
        this.customizedFormTitle = 'Update superheroe';
        this.isFormOpen = true;
        this.customizedFormAction = FormAction.edit;
        this.customizedFormItemToModify = superheroe;
        this.customFields.forEach((customField => 
            {
                if(customField.name === 'id'){ 
                    customField.value = superheroe.id
                } else if(customField.name === 'name'){
                    customField.value = superheroe.name;
                } else if(customField.name === 'superpower'){
                    customField.value = superheroe.superpower;
                }
            }
            ));
    }

    public deleteSuperheroe(id: number): void {
        this.isConfirmDeleteFormOpen = true;
        this.customizedFormItemToDelete = id;
    }

    public updateFieldValues(updatedFieldValues: SuperHeroeInterface): void {

        if (this.customizedFormAction === FormAction.create) {

            let randomNumber = this.getRandomIntInclusive();
            let superhero: SuperHeroeInterface = {
                id: randomNumber,
                name: updatedFieldValues.name,
                superpower: updatedFieldValues.superpower
            };

        this.dataService.create(superhero);
        }
        else if (this.customizedFormAction === FormAction.edit) {

            let superhero: SuperHeroeInterface = {
                id: this.customizedFormItemToModify.id,
                name: updatedFieldValues.name,
                superpower: updatedFieldValues.superpower
            };
            this.update(this.customizedFormItemToModify.id, superhero);
        }
        this.customizedFormAction = FormAction.none;
 
    }
    
    public hasBeenFormClosed(): void {
        this.isFormOpen = false;
    }

    public hasBeenConfirmDeleteFormClosed(): void {
        this.isConfirmDeleteFormOpen = false;
    }    

    public getRandomIntInclusive(min: number = 150, max: number = 999): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

}