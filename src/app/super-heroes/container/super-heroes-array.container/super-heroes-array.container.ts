import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from "../../../@shared/service";
import { LoaderService } from "../../../@core/service";
import { SuperHeroeCustomFields } from '../../model';
import { FormAction, FormCustomFieldInterface } from '../../../@shared/model';
import { SuperHeroeInterface } from "../../../@shared/model/super-heroe.interface";
import { Subject, Subscription, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'app-super-heroes-array-container',
    templateUrl: './super-heroes-array.container.html',
    styleUrls: ['./super-heroes-array.container.scss'],
})

export class SuperHeroesArrayContainer implements OnInit, OnDestroy {

    public title: string;
    public itemsArray!: Array<SuperHeroeInterface>;

    public customizedFormTitle: string;
    public customFields: Array<FormCustomFieldInterface>;
    public isFormModal: boolean;
    public isFormOpen: boolean;
    public customizedFormAction: FormAction;
    public customizedFormItemToModify: number;

    public customizedFormItemToDelete: number;
    public isConfirmDeleteFormOpen: boolean;

    public dataServiceSubject$: Subject<Array<SuperHeroeInterface>>;
    public dataServiceSubjectSubscription!: Subscription;

    constructor(private dataService: DataService, private loaderService: LoaderService) {

        this.title = 'SuperHeroe Array Container';
        this.itemsArray = this.dataService.superheroes;
        this.customizedFormTitle = '';
        this.customFields = SuperHeroeCustomFields;
        this.isFormModal = true;
        this.isFormOpen = false;
        this.customizedFormAction = FormAction.none;
        this.customizedFormItemToModify = 0;
        this.customizedFormItemToDelete = 0;
        this.isConfirmDeleteFormOpen = false;
        this.dataServiceSubject$ = this.dataService.dataServiceSubject$;
    }

    ngOnInit(): void {
        this.getDataServiceSubscription();
    }

    ngOnDestroy(): void {
        this.removeDataServiceSubscription();
    }

    public getDataServiceSubscription(): void {
        this.dataServiceSubjectSubscription = this.dataServiceSubject$
            .pipe(
                catchError(err => {
                    console.error(err);
                    return throwError(() => new Error())
                })
            )
            .subscribe({
                next: (data: Array<SuperHeroeInterface>) => {
                    this.itemsArray = data;
                },
                error: (error: any) => {
                    console.error(error)
                },
                complete: () => {
                }
            });
    }

    public removeDataServiceSubscription(): void {
        this.dataServiceSubjectSubscription.unsubscribe();
    }

    public createSuperheroe(): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            this.customizedFormTitle = 'Create superheroe';
            this.isFormOpen = true;
            this.customizedFormAction = FormAction.create;
            this.customFields.forEach((customField => {
                if (customField.name === 'id') {
                    customField.value = 'new'
                } else if (customField.name === 'name') {
                    customField.value = '';
                } else if (customField.name === 'superpower') {
                    customField.value = '';
                }
            }
            ));
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public readAll(): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            this.dataService.readAll()
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public readSuperheroeById(idString: string): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            var idNumber: number = +idString;
            this.dataService.readById(idNumber);
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }


    public readSuperheroeByValue(value: string): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            this.dataService.readByValue(value);
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public updateSuperheroe(superheroe: any): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            this.customizedFormTitle = 'Update superheroe';
            this.isFormOpen = true;
            this.customizedFormAction = FormAction.edit;
            superheroe = <SuperHeroeInterface>superheroe;
            this.customizedFormItemToModify = superheroe.id;
            this.customFields.forEach((customField => {
                if (customField.name === 'id') {
                    customField.value = superheroe.id;
                } else if (customField.name === 'name') {
                    customField.value = superheroe.name;
                } else if (customField.name === 'superpower') {
                    customField.value = superheroe.superpower;
                }
            }
            ));
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public deleteSuperheroe(id: number): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            this.isConfirmDeleteFormOpen = true;
            this.customizedFormItemToDelete = id;
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public updateFieldValues(updatedFieldValues: SuperHeroeInterface): void {
        this.loaderService.emitLoadStatus("visible");
        setTimeout(() => {
            if (this.customizedFormAction === FormAction.create) {
                let randomNumber = this.getRandomIntInclusive();
                let superhero: SuperHeroeInterface = {
                    id: randomNumber,
                    name: updatedFieldValues.name,
                    superpower: updatedFieldValues.superpower
                };
                this.dataService.create(superhero);
            }
            else if (this.customizedFormAction === FormAction.edit) {
                let superhero: SuperHeroeInterface = {
                    id: this.customizedFormItemToModify,
                    name: updatedFieldValues.name,
                    superpower: updatedFieldValues.superpower
                };
                this.update(this.customizedFormItemToModify, superhero);
            }
            this.customizedFormAction = FormAction.none;
            this.loaderService.emitLoadStatus("not-visible");
        }, 500);
    }

    public update(id: number, item: SuperHeroeInterface): void {
        this.dataService.update(id, item);
    }

    public delete(id: number): void {
        this.dataService.delete(id);
        this.customizedFormItemToDelete = 0;
    }

    public hasBeenFormClosed(): void {
        this.isFormOpen = false;
    }

    public hasBeenConfirmDeleteFormClosed(): void {
        this.isConfirmDeleteFormOpen = false;
    }

    public getRandomIntInclusive(min: number = 150, max: number = 999): number {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

}