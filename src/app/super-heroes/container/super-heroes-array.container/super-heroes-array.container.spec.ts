import { ComponentFixture, TestBed, getTestBed, fakeAsync, tick, async, waitForAsync } from '@angular/core/testing';
import { DataService } from "../../../@shared/service";
import { LoaderService } from "../../../@core/service";
import { FormAction, FormCustomFieldInterface } from '../../../@shared/model';
import { SuperHeroeInterface } from "../../../@shared/model/super-heroe.interface";
import { SuperHeroeCustomFields } from '../../model';
import { SuperHeroesArrayContainer } from './super-heroes-array.container';
import { SuperHeroesData } from '../../../@config/data/super-heroes-data.const';
import {
  dummySuperHeroeArray,
  dummySuperHeroeArrayById,
  dummySuperHeroeArrayByValue,
  dummySuperHeroeUpdated
} from '../../../@config/data/data-service-test-fake-data';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SuperHeroesArrayContainer Component', () => {
  let injector: TestBed;
  let component: SuperHeroesArrayContainer;
  let fixture: ComponentFixture<SuperHeroesArrayContainer>;
  let dataService: DataService;
  let loaderService: LoaderService

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SuperHeroesArrayContainer],
      providers: [DataService, LoaderService],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
    injector = getTestBed();
    dataService = TestBed.inject(DataService);
    loaderService = TestBed.inject(LoaderService);
    fixture = TestBed.createComponent(SuperHeroesArrayContainer);
    component = fixture.componentInstance;
    dataService.superheroes = dummySuperHeroeArray;
    fixture.detectChanges();
  });

  afterEach(() => {
    component = null;
    fixture = null;
    injector = null;
    dataService = null;
    loaderService = null;
  })

  it('should have a valid constructor', () => {

    expect(dataService).not.toBeNull();
    expect(dataService).toBeInstanceOf(DataService);
    expect(loaderService).not.toBeNull();
    expect(loaderService).toBeInstanceOf(LoaderService);
    expect(component).not.toBeNull();
    expect(component).toBeInstanceOf(SuperHeroesArrayContainer);
  });

  it('should instanciate injected services', () => {
    expect(dataService).toBeInstanceOf(DataService);
    expect(loaderService).toBeInstanceOf(LoaderService);
  });

  it('should initialize properties', () => {

    let title: string = 'SuperHeroe Array Container';
    expect(title).toEqual(component.title);

    let itemsArray: Array<SuperHeroeInterface> = SuperHeroesData;
    expect(itemsArray).toEqual(component.itemsArray);

    let customizedFormTitle: string = '';
    expect(customizedFormTitle).toEqual(component.customizedFormTitle);

    let customFields: Array<FormCustomFieldInterface> = SuperHeroeCustomFields;
    expect(customFields).toEqual(component.customFields);

    let isFormModal: boolean = true;
    expect(isFormModal).toEqual(component.isFormModal);

    let isFormOpen: boolean = false;
    expect(isFormOpen).toEqual(component.isFormOpen);

    let customizedFormAction: FormAction = FormAction.none;
    expect(customizedFormAction).toEqual(component.customizedFormAction);

    let customizedFormItemToModify: number = 0;
    expect(customizedFormItemToModify).toEqual(component.customizedFormItemToModify);

    let customizedFormItemToDelete: number = 0;
    expect(customizedFormItemToDelete).toEqual(component.customizedFormItemToDelete);

    let isConfirmDeleteFormOpen: boolean = false;
    expect(isConfirmDeleteFormOpen).toEqual(component.isConfirmDeleteFormOpen);

    expect(component.dataServiceSubject$).not.toBeNull();
  });

  it('should call ngOnInit correctly', () => {

    const methodSpy = spyOn(component, 'getDataServiceSubscription');
    fixture.detectChanges();
    component.ngOnInit();
    expect(methodSpy).toHaveBeenCalled();
  });

  it('should call ngOnDestroy correctly', () => {

    const methodSpy = spyOn(component, 'removeDataServiceSubscription');
    fixture.detectChanges();
    component.ngOnDestroy();
    expect(methodSpy).toHaveBeenCalled();
  });

  it('should call createSuperheroe correctly', fakeAsync(() => {

    /* ORDER MATTERS, methodSpy2 must be executed before methodSpy OR TEST FAILS */

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.createSuperheroe();
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const methodSpy = spyOn(component, 'createSuperheroe');
    component.createSuperheroe();
    expect(methodSpy).toHaveBeenCalled();

      let customizedFormTitle: string = 'Create superheroe';
      expect(customizedFormTitle).toEqual(component.customizedFormTitle);

      let isFormOpen: boolean = true;
      expect(isFormOpen).toEqual(component.isFormOpen);

      let customizedFormAction: FormAction = FormAction.create;
      expect(customizedFormAction).toEqual(component.customizedFormAction);

      let customFieldValueIsNew: string | number = 'new';
      let customFieldValueIsEmpty: string | number = '';
     
      component.customFields.forEach((customField => {
        if (customField.name === 'id') {
          expect(customFieldValueIsNew).toEqual(customField.value);
        } else if (customField.name === 'name') {
          expect(customFieldValueIsEmpty).toEqual(customField.value);
        } else if (customField.name === 'superpower') {
          expect(customFieldValueIsEmpty).toEqual(customField.value);
        }
      }
      ));
 
  }));

  it('should call readAll correctly', fakeAsync(() => {

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.readAll();
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const methodSpy = spyOn(component, 'readAll');
    component.readAll();
    expect(methodSpy).toHaveBeenCalled();

    let itemsArray: Array<SuperHeroeInterface> = SuperHeroesData;
    expect(component.itemsArray).not.toBeNull();
    expect(itemsArray).toEqual(component.itemsArray);
  }));

  it('should call readSuperheroeById correctly', fakeAsync(() => {

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.readSuperheroeById('3');
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const spy = spyOn(component, 'readSuperheroeById');
    component.readSuperheroeById('3');
    expect(spy).toHaveBeenCalled();

    let itemsArray: Array<SuperHeroeInterface> = dummySuperHeroeArrayById;
    expect(component.itemsArray).not.toBeNull();
    expect(itemsArray).toEqual(component.itemsArray);
 
  }));

  it('should call readSuperheroeByValue correctly', fakeAsync(() => {

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.readSuperheroeByValue('bAt');
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const spy = spyOn(component, 'readSuperheroeByValue');
    component.readSuperheroeByValue('bAt');
    expect(spy).toHaveBeenCalled();

    let itemsArray: Array<SuperHeroeInterface> = dummySuperHeroeArrayByValue;
    expect(component.itemsArray).not.toBeNull();
    expect(itemsArray).toEqual(component.itemsArray);
  }));

  it('should call updateSuperheroe correctly', fakeAsync(()  => {

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.updateSuperheroe(dummySuperHeroeUpdated);
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const spy = spyOn(component, 'updateSuperheroe');
    component.updateSuperheroe(dummySuperHeroeUpdated);
    expect(spy).toHaveBeenCalled();

    let customizedFormTitle: string = 'Update superheroe';
    expect(customizedFormTitle).toEqual(component.customizedFormTitle);
    let isFormOpen: boolean = true;
    expect(isFormOpen).toEqual(component.isFormOpen);
    let customizedFormAction: FormAction = FormAction.edit;
    expect(customizedFormAction).toEqual(component.customizedFormAction);
    let customizedFormItemToModify: number = 4;
    expect(customizedFormItemToModify).toEqual(component.customizedFormItemToModify);

    let customFieldValueId: string | number = 4;
    let customFieldValueName: string | number = 'Batman';
    let customFieldValueSuperpower: string | number = 'intelligence';

      component.customFields.forEach((customField => {
        if (customField.name === 'id') {
          expect(customFieldValueId).toEqual(customField.value);
        }
        if (customField.name === 'name') {
          expect(customFieldValueName).toEqual(customField.value);
        }
        if (customField.name === 'superpower') {
          expect(customFieldValueSuperpower).toEqual(customField.value);
        }
      }
      ));

  }));

  it('should call deleteSuperheroe correctly', fakeAsync(()  => {

    const methodSpy2 = spyOn(loaderService, 'emitLoadStatus');
    component.deleteSuperheroe(4);
    tick(500);
    fixture.detectChanges();
    expect(methodSpy2).toHaveBeenCalled();

    const spy = spyOn(component, 'deleteSuperheroe');
    component.deleteSuperheroe(4);
    expect(spy).toHaveBeenCalled();

    let isConfirmDeleteFormOpen: boolean = true;
    expect(isConfirmDeleteFormOpen).toEqual(component.isConfirmDeleteFormOpen);
    let customizedFormItemToDelete: number = 4;
    expect(customizedFormItemToDelete).toEqual(component.customizedFormItemToDelete);

  }));

  it('should call updateFieldValues on create correctly', fakeAsync(() => {

    component.customizedFormAction = FormAction.create;

    let spyService = spyOn(dataService, 'create');
    component.updateFieldValues(dummySuperHeroeUpdated);
    tick(1000);
    fixture.detectChanges();
    expect(spyService).toHaveBeenCalled();

    let spyComponent = spyOn(component, 'updateFieldValues');
    component.updateFieldValues(dummySuperHeroeUpdated);
    expect(spyComponent).toHaveBeenCalled();

  }));

  it('should call updateFieldValues on edit correctly', fakeAsync(() => {

    component.customizedFormAction = FormAction.edit;

    let spyService = spyOn(dataService, 'update');
    component.updateFieldValues(dummySuperHeroeUpdated);
    tick(1000);
    fixture.detectChanges();
    expect(spyService).toHaveBeenCalled();

    let spyComponent = spyOn(component, 'updateFieldValues');
    component.updateFieldValues(dummySuperHeroeUpdated);
    expect(spyComponent).toHaveBeenCalled();

  }));

  it('should call hasBeenFormClosed correctly', () => {

    component.hasBeenFormClosed();
    let isFormOpen: boolean = false;
    expect(isFormOpen).toEqual(component.isFormOpen);
  });


  it('should call hasBeenConfirmDeleteFormClosed correctly', () => {

    component.hasBeenConfirmDeleteFormClosed();
    let isConfirmDeleteFormOpen: boolean = false;
    expect(isConfirmDeleteFormOpen).toEqual(component.isConfirmDeleteFormOpen);
  });

  it('should call update correctly', () => {

    let spyComponent = spyOn(component, 'update');
    component.update(11, dummySuperHeroeUpdated);
    expect(spyComponent).toHaveBeenCalled();
  });

  it('should call delete correctly', () => {

    let spyComponent = spyOn(component, 'delete');
    component.delete(3);
    expect(spyComponent).toHaveBeenCalled();
    expect(component.customizedFormItemToDelete).toEqual(0);
  });






  it('should get confirm-delete-dialog', () => {
    const element = fixture.debugElement.query(By.css('#confirm-delete-dialog'));
    expect(element).toBeTruthy(); 

    expect(element.properties['customizedFormTitle']).toEqual('');
    expect(element.properties['customizedFormText']).toEqual('Are you sure to delete this hero?');
    expect(element.properties['isFormModal']).toEqual(true);
    expect(element.properties['isFormOpen']).toEqual(false);

    let spyComponent = spyOn(component, 'delete');
    component.customizedFormItemToDelete = 5;
    element.triggerEventHandler('updatedFieldValues');
    expect(spyComponent).toHaveBeenCalledWith(5);

    spyComponent = spyOn(component, 'hasBeenConfirmDeleteFormClosed');
    element.triggerEventHandler('hasBeenFormClosed');
    expect(spyComponent).toHaveBeenCalled();  
  });

  it('should get new-update-dialog', () => {
    const element = fixture.debugElement.query(By.css('#new-update-dialog'));
    expect(element).toBeTruthy(); 

    expect(element.properties['customizedFormTitle']).toEqual('');
    expect(element.properties['customFields']).toEqual(component.customFields);
    expect(element.properties['isFormModal']).toEqual(true);
    expect(element.properties['isFormOpen']).toEqual(false);

    let spyComponent = spyOn(component, 'updateFieldValues');
    element.triggerEventHandler('updatedFieldValues', dummySuperHeroeUpdated);
    expect(spyComponent).toHaveBeenCalledWith(dummySuperHeroeUpdated);

    spyComponent = spyOn(component, 'hasBeenFormClosed');
    element.triggerEventHandler('hasBeenFormClosed');
    expect(spyComponent).toHaveBeenCalled();  
  });

  it('should get controls-container', () => {
    const element = fixture.debugElement.query(By.css('#controls-container'));
    expect(element).toBeTruthy(); 
  });

  it('should get logo', () => {
    const element = fixture.debugElement.query(By.css('#logo'));
    expect(element).toBeTruthy(); 
  });

  it('should get title', () => {
    const element = fixture.debugElement.query(By.css('#title'));
    expect(element).toBeTruthy(); 
    expect(component.title).toBe('SuperHeroe Array Container');

  });

  it('should get controls-grid', () => {
    const element = fixture.debugElement.query(By.css('#controls-grid'));
    expect(element).toBeTruthy(); 
  });

  it('should get mat-grid1', () => {
    const element = fixture.debugElement.query(By.css('#mat-grid1'));
    expect(element).toBeTruthy(); 
  });

  it('should get button1', () => {
    const element = fixture.debugElement.query(By.css('#button1'));
    expect(element).toBeTruthy(); 

    expect(element.properties['inputTitle']).toEqual('Create');
    let spyComponent = spyOn(component, 'createSuperheroe');
    element.triggerEventHandler('outputClick');
    expect(spyComponent).toHaveBeenCalled();
  });

  it('should get the button2', () => {
    const element = fixture.debugElement.query(By.css('#button2'));
    expect(element).toBeTruthy(); 

    expect(element.properties['inputTitle']).toEqual('Read All');
    let spyComponent = spyOn(component, 'readAll');
    element.triggerEventHandler('outputClick');
    expect(spyComponent).toHaveBeenCalled();
  });

  it('should get mat-grid2', () => {
    const element = fixture.debugElement.query(By.css('#mat-grid2'));
    expect(element).toBeTruthy(); 
  });

  it('should get search1', () => {
    const element = fixture.debugElement.query(By.css('#search1'));
    expect(element).toBeTruthy(); 
  }); 

  it('should get the button3', () => {
    const button = fixture.debugElement.query(By.css('#button3'));
    expect(button).toBeTruthy(); 
    expect(button.properties['inputTitle']).toEqual('Search By Id');
    
    const input = fixture.debugElement.query(By.css('#superheroeid'));
    expect(input).toBeTruthy();  
    input.nativeElement.value = '11';
  
    let spyComponent = spyOn(component, 'readSuperheroeById');
    button.triggerEventHandler('outputClick', input.nativeElement.value);
    expect(spyComponent).toHaveBeenCalledWith('11'); 
  });

  it('should get search2', () => {
    const element = fixture.debugElement.query(By.css('#search2'));
    expect(element).toBeTruthy(); 
  }); 

  it('should get the button4', () => {
    const button = fixture.debugElement.query(By.css('#button4'));
    expect(button).toBeTruthy(); 
    expect(button.properties['inputTitle']).toEqual('Search By Value');
    
    const input = fixture.debugElement.query(By.css('#superherovalue'));
    expect(input).toBeTruthy();  
    input.nativeElement.value = 'antonioman';
  
    let spyComponent = spyOn(component, 'readSuperheroeByValue');
    button.triggerEventHandler('outputClick', input.nativeElement.value);
    expect(spyComponent).toHaveBeenCalledWith('antonioman'); 
  });

  it('should get the data-table', () => {
    const element = fixture.debugElement.query(By.css('#data-table'));
    expect(element).toBeTruthy(); 
    expect(element.properties['inputArray']).toEqual(dummySuperHeroeArray);

    let spyComponent = spyOn(component, 'updateSuperheroe');
    element.triggerEventHandler('outputUpdateClick', 8);
    expect(spyComponent).toHaveBeenCalledWith(8); 

    spyComponent = spyOn(component, 'deleteSuperheroe');
    element.triggerEventHandler('outputDeleteClick', 8);
    expect(spyComponent).toHaveBeenCalledWith(8); 
  });  

});