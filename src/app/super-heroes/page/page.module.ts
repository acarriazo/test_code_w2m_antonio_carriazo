import { NgModule } from '@angular/core';
import { SuperHeroesArrayPage, SuperHeroesInMemoryPage } from '.';
import { ContainerModule } from '../container/container.module';

@NgModule({
  declarations: [
    SuperHeroesArrayPage, SuperHeroesInMemoryPage
  ],
  imports: [ContainerModule],
  providers: [],
  exports: [ 
    SuperHeroesArrayPage, SuperHeroesInMemoryPage
   ]
})
export class PageModule { }