import { NgModule } from '@angular/core';
import { SuperHeroesRoutingModule } from './super-heroes-routing.module';
import { PageModule } from './page/page.module';

@NgModule({
  declarations: [],
  imports: [ 
    PageModule,
    SuperHeroesRoutingModule
   ],
  providers: [],
  exports: [ 
    PageModule,
    SuperHeroesRoutingModule 
  ]
})
export class SuperHeroesModule { }