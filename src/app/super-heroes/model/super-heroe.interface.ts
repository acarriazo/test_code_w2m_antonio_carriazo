import { SuperpowerType } from '.';

export interface SuperheroeInterface 
    {
        id: number;
        name: string;
        superpower: SuperpowerType;
    }