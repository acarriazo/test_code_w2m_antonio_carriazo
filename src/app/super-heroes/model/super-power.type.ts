export type SuperpowerType =  
    'invisibility' | 
    'strength' | 
    'speed' | 
    'intelligence' |
    'stealth' |
    'magnetism' |
    'invincible' |
    'inmortal'
    ;