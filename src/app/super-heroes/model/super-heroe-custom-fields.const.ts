import { Validators } from '@angular/forms';
import { SuperPowerValidator } from '../validator';

export const SuperHeroeCustomFields = [
  {
      name: 'id',
      type: 'text',
      value: null,
      validators: [],
      disabled: true
  },
  {
      name: 'name',
      type: 'text',
      value: null,
      validators: [Validators.required, Validators.maxLength(20) ],
      disabled: false
  },
  {
      name: 'superpower',
      type: 'text',
      value: null,
      validators: [Validators.required, SuperPowerValidator.superPower],
      disabled: false
  }
];

